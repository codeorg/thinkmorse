# thinkmorse
Forked from https://github.com/theritvars/thinkmorse

Python script for flashing the ThinkPad lid LED in Morse code

## Prerequisites
```bash
sudo modprobe -r ec_sys
sudo modprobe ec_sys write_support=1
```

## How to use
```
sudo python main.py
```

